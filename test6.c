#include <stdio.h>

int main()
{
	int i, j, n, highest;
	int marks[5][3];
	for (i = 0; i < 5; i++)
	{
		for (j = 0; j < 3; j++)
		{
			printf("\nEnter [%d][%d] elements of the array:\n", i, j);
			scanf("%d", &marks[i][j]);
		}
	}

	for (j = 0; j< 3; j++)
	{
		highest = 0;
		for (i = 0; i< 5; i++)
		{
			if (highest < marks[i][j])
				highest = marks[i][j];
		}
	
		printf("\nHighest marks of subject in the given array is %d", highest);
}	
	return 0;
}
