#include <stdio.h>

struct employee
{
	char name[30];
	int empId;
	char designation[30];
	char department[30];
	float experience;
	float salary;
};

int main()
{
	struct employee emp;

	printf("\nEnter details :\n");
	printf("Name ?:");	gets(emp.name);
	printf("ID ?:");	scanf("%d", &emp.empId);
	printf("Designation ?:");	scanf("%s", emp.designation);
	printf("Department?:");	scanf("%s", emp.department);
	printf("Experience ?:");	scanf("%f", &emp.experience);
	printf("Salary ?:");	scanf("%f", &emp.salary);
   printf("\n");
   
	printf("\nEntered detail is:");
	printf("\nName: %s", emp.name);
	printf("\nId: %d", emp.empId);
	printf("\nDesignation: %s", emp.designation);
	printf("\nDepartment: %s", emp.department);
	printf("\nExperience: %f", emp.experience);
	printf("\nSalary: %f\n", emp.salary);
	return 0;
}
